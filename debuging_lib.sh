#!/bin/bash
#
# This file contains all the functions that are used on the launch.sh script
# These functions are responsible for starting the development services with or without debugging mode
# The debugging mode has exactly the same services with the main difference that are running 
# on a tmux session with the name of "debug" and they don't keep logs 

run_karpathos(){
  python3 -m karpathos run --port $1
}

run_serve_batch(){
  python3 -m karpathos serve_batch
}

run_serve_push(){
  python3 -m karpathos serve_push
}

run_service_on_tmux(){
  OPTIND=1
  split=0
  while getopts abcgpstvn:o: opt; do	
    case "$opt" in
      a) 
        module="run_karpathos $KARPATHOS_PORT"
        ;;
      b) 
        module='run_serve_batch'
        ;;
      c) 
        module='run_worker cpu'
        ;;
      g) 
        module='run_worker gpu'
        ;;
      p)
        module='run_serve_push'
        ;;
      s) 
        module="run_karpathos $KARPATHOS_REALTIME_PORT"
        ;;
      t)
        module='run_worker bot'
        ;;
      v)
        module='run_viewer_with_debug'
        ;;
      n)
        pane=$OPTARG
        ;;
      o)
        split="-$OPTARG"
        ;;
    esac
  done
  shift $((OPTIND -1))
  tmux send-keys 'source ~/.custom/debuging_lib.sh' 'C-m'
  tmux send-keys "$module" 'C-m'
  
  if [[ $pane ]]; then
    tmux select-pane -t $pane
  fi
  if [[ $split != 0 ]]; then
    tmux split-window $split
  fi
}

run_services(){
  if [[ "$#" -gt 0 ]]; then
    if [[ $1 != '--api' ]]; then
      echo "run api"
      run_karpathos $KARPATHOS_PORT &
    fi
    if [[ $1 != '--batch' ]]; then
      echo "run batch"
      run_serve_batch &
    fi
    if [[ $1 != '--cpu' ]]; then
      echo "run cpu"
      run_worker cpu --og &
    fi
    if [[ $1 != '--gpu' ]]; then 
      echo "run gpu"
      run_worker gpu --log &
    fi
    if [[ $1 != '--push' ]]; then
      echo "run push"
      run_serve_push &
    fi
    if [[ $1 != '--socket' ]]; then
      echo "run socket"
      run_karpathos $KARPATHOS_REALTIME_PORT &
    fi
    if [[ $1 != '--bot' ]]; then 
      echo "run bot"
      run_worker bot --log &
    fi
    if [[ $1 != '--viewer' ]]; then 
      echo "run viewer"
      run_viewer_with_restart &
    fi
  fi
}

debug_all(){
  tmux new-session -d -s debug
  tmux select-window -t debug:0
  run_service_on_tmux -a -n 0 -o 'h'
  run_service_on_tmux -s -n 1 -o 'v'
  run_service_on_tmux -v -n 2 -o 'h'
  run_service_on_tmux -g -n 1 -o 'h'
  run_service_on_tmux -c -n 0 -o 'h'
  run_service_on_tmux -b -n 1 -o 'v'
  run_service_on_tmux -p -n 0 -o 'v'
  run_service_on_tmux -t
  open_tmux_session
}

debug_service(){
  if [[ "$#" -gt 0 ]]; then
    tmux new-session -d -s debug
    tmux select-window -t debug:0
    tmux send-keys 'source ~/.custom/debuging_lib.sh' 'C-m'
    tmux send-keys "run_services $1" 'C-m'
    tmux select-pane -t 0
    tmux split-window -h 
    case $1 in
      --api) run_service_on_tmux -a ;;
      --batch) run_service_on_tmux -b ;;
      --cpu) run_service_on_tmux -c ;;
      --gpu) run_service_on_tmux -g ;;
      --push) run_service_on_tmux -p ;;
      --socket) run_service_on_tmux -s ;;
      --bot) run_service_on_tmux -t ;;
      --viewer) run_service_on_tmux -v ;;
    esac
  fi
  tmux break-pane
  open_tmux_session
}

run_debug_cli(){
  while true; do
    read -p "Do you wish to debug? [yN]:" yn
    case $yn in
      [Yy]* ) read -p "What? a) api b) batch c) cpu g) gpu p) push s) socket t) bot v) viewer x) all:" opt
        case $opt in
          a) debug_service --api
            break;;
          b) debug_service --batch
            break;;
          c) debug_service --cpu
            break;;
          g) debug_service --gpu
            break;;
          p) debug_service --push
            break;;
          s) debug_service --socket
            break;;
          t) debug_service --bot
            break;;
          v) debug_service --viewer
            break;;
          x) debug_all
            break;;
          *) echo "Wrong option."
            exit;;
        esac
        break;;
      [Nn]* ) exit;;
      * ) echo "Please answer yes or no.";;
    esac
  done
}

run_worker() {
  if [[ "$#" -gt 0 ]]; then
      case $2 in
          -l|--log) log=1; ;;
          --) ;;
      esac
  fi

  if [[ $log -eq 1 ]]; then
    rq worker $1 \
      --worker-class=karpathos.jobs.Worker \
      --job-class=karpathos.jobs.Job \
      --queue-class=karpathos.jobs.Queue | tee worker.$1.log
  else
    rq worker $1 \
      --worker-class=karpathos.jobs.Worker \
      --job-class=karpathos.jobs.Job \
      --queue-class=karpathos.jobs.Queue
  fi
}

run_viewer_with_debug(){
  python3 -m karpathos.utils.set_host_redis
  gdb ../build/thera/viewer/viewer
  cowsay -d Viewer has crashed... 
}

run_viewer_with_restart(){
  while true
  do
      python3 -m karpathos.utils.set_host_redis
      ../build/thera/viewer/viewer | tee viewer.log
      cowsay -d Viewer has crashed... 
      cp viewer.log viewer-crash.log
    sleep 1
  done
}
check_tmux_session(){
  tmux has-session -t debug 2>/dev/null
}
open_tmux_session(){
  check_tmux_session
  if [ $? != 1 ]; then
    tmux a -t debug
  fi
}

stop_debug_if_exist(){
  check_tmux_session
  if [ $? != 1 ]; then
    echo "tmux session debug exist"
    tmux kill-session -t debug
  fi
}
