
move_folder(){
 if [ $PWD != $HOME/.custom ]; then
  mv ../dotfiles $HOME/.custom
  echo "############"
  echo "folder moved"
  echo "############"
 fi
}

append_bashrc(){
if ! grep -q 'for file in "$HOME/.custom"/*' "$HOME/.bashrc"; then 
tee -a <<- 'EOF' $HOME/.bashrc 
  
# Source each file inside the .custom folder

for file in "$HOME/.custom"/*
do 
  . $file
done
EOF

echo "###############"
echo "added on bashrc"
echo "###############"
fi
}

delete_installer(){
  rm -rf ./install.sh
  echo "###############"
  echo "installer removed"
  echo "###############"
}

move_folder
append_bashrc
delete_installer
