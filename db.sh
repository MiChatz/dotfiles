#!/bin/bash
db(){
  case $1 in
    drop) 
      su - postgres -c "dropdb $2" 
      echo "###############"
      echo "db $2 is droped"
      echo "###############"
      ;;
    create) 
      su - postgres -c "createdb $2"
      echo "####################"
      echo "db $2 is created"
      echo "info: run db migrate"
      echo "####################"
      ;;
    migrate)
      cd /workspace/thera/server/
      PYTHONPATH=. alembic -x db=development upgrade head
      echo "#################"
      echo "db migarted"
      echo "info: run db seed thera"
      echo "#################"
      ;;
    seed)
      cd /workspace/thera/server/ 
      case $2 in
        thera)
          python3 -m karpathos init_db
          echo "############"
          echo "thera seeded"
          echo "############"
          ;;
        beams) 
          python3 -m karpathos seed_beams
          echo "############"
          echo "beams seeded"
          echo "############"
          ;;
        names)
          if [ $3 ]; then
            python3 -m karpathos set_custom_structure_names $3
            echo "############"
            echo "names seeded"
            echo "############"
          else
            echo "#######################################"
            echo "missing names files"
            echo "info: run db seed names <path_to_file>"
            echo "#######################################"
          fi
          ;;
      esac
      ;;
    connect)
      case $2 in
        local)
          su - postgres -c "pgcli"
          ;;
        remote)
          source ~/.custom/.pgpass
          pgcli -U michalis -h kovu -p 15432 theradata
          ;;
      esac
      ;;
    *) echo "wrong argument"
  esac
}

