#!/bin/bash

build(){
  case $1 in
    thera) 
      rm -rf /workspace/thera/build
      mkcd /workspace/thera/build
      cmake -DPYTHON_EXECUTABLE=/usr/bin/python3.8 -DCMAKE_BUILD_TYPE=Release ../server &&  make -j$2
      echo "####################"
      echo "thera build finished"
      echo "####################"
      ;;
    mxnet) 
      rm -rf /workspace/thera/server/third_party/mxnet/build
      mkcd /workspace/thera/server/third_party/mxnet/build
      bash ../run_cmake_mxnet.sh && make -j$2
      echo "####################"
      echo "mxnet build finished"
      echo "####################"
      ;;
    *) echo "wrong argument"
  esac
}

