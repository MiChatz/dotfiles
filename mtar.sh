#!/bin/bash

mtar(){
  tar -cvzf "$1.tar.gz" "$1"
  echo "###################"
  echo "tar $1 is generated"
  echo "###################"
}
